## Hardware specific issues

- One BeagleBone pin can have different functions (can be either SPI pin or HDMI pin). Need to load appropriate "device tree overlay" to Cape Manager.

- Device tree overlay for Debian 7 doesn't work with Debian 8!
- Cannot load overlay on-the-fly on Debian 8 → Some libraries stop workings.
