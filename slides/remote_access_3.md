## Remote access via Internet


+ [Hamachi](https://www.vpn.net/) (not a FOSS): Easy to install and configure. Limit 5 users for free package. Windows version is very unstable (no connection).
+ [SoftEther VPN](https://www.softether.org/): Open source. SSL certificate is optional. Many layers of configuration.
+ [WireGuard](https://www.wireguard.com/): Fast (running in Linux kernel space), lightweight, easy to install (haven't tested on Windows, Android). Use SSH-like key files, instead of SSL certificate.
