## Remote access via Internet

Method 2: Port forwarding on main router.

- Best speed (no intermediate server).
- Rely on external factor (customer own devices).
- IPv4 address exhaustion &rarr; non-public IP address.
