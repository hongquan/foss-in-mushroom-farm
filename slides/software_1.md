## Software stack

- Software for gateway is built with Python, Django.

- Usual stack for a Python web app: PostgreSQL, Redis, Nginx. Version 2 adding [InfluxDB](https://www.influxdata.com) for time-series database.

- Some configuration to integrate well to OS: Device Tree overlay for ARM Linux, _systemd_ service definition.
