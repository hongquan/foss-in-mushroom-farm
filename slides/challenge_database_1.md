<!-- .slide: style="text-align: left;"> -->

## Database issues

First version: PostgreSQL for sensor data

_Condition_ table:

```
- id (primary key)
- node_id (foreign key to Node table)
- temperature
- humidity
- date_measured
```

- Acquire sensor data every 10s. Each node produces 10x6x60x24 = 86400 records every day.
- PostgreSQL often slows down the system with 100% CPU.
