## Database issues


InfluxDB

- Day by day, InfluxDB consumes **a lot** of memory.
- Not much gain: The performance of querying recent data is not better than indexed Postgres.
