## Hardware specific issues

- Using hardware ports (GPIO, PWM, SPI) needs root permission.
- Many ports is not controlled by `udev` (the files are under _/sys/class/_, not _/dev/_). Cannot use udev rule to change permission.

Solution:

Write a tool to change permission of the ports. Use `systemd` service to execute the tool at startup.
