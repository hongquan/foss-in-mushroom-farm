## Hardware overview

- Difference from other normal IoT system:

    + No cloud server.

    + The gateway doesnot only collect sensor data and transfer to cloud server. The gateway gives decision itself, not receive from any cloud server.

- Why different?

    + Lack of resource (infrastructure, human etc).
    + Serve as in-house project, with small scale.
