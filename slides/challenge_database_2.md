## Database issues

Troubleshootings:

- Use [pghero](https://github.com/ankane/pghero), [pg_stat_statements](https://www.postgresql.org/docs/9.4/static/pgstatstatements.html) extension, Django's SQL debug feature to find out which query is the slowest.
- Use PostgreSQL `EXPLAIN ANALYZE` to find out which operation is done and how long it takes under that query.
- Find out that PostgeSQL often has to scan all rows in the table for some queries.
