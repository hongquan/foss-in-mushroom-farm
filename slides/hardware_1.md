## Hardware overview

![Hardware](img/Hardware.png) <!-- .element: style="height: 280px; margin-bottom: -20px" -->

- Gateway: BeagleBone Black running Debian Linux.
- Sensor nodes, based on Arduino, ESP8266, ESP32.
- Actuators (humidifier, LED, air-cond, cooling pad, fan)
