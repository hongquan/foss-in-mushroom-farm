## Software stack

Main components:

- ControlView: Web UI. Display sensor values. Interface for farmer to control actuator, or to schedule. Export data to CSV.
- Collector: Collect data from sensor nodes, save to database.
- ControlCenter: Control the actuators in "automatic" mode.

Those actually share the same code base, stay in the same source tree.
