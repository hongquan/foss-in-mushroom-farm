## Database issues

Solve:
- Build PostgreSQL index based on _date_measured_ field, change the SQL query to drive engine to scan index.
- Only build index for the most recent records → Need to make a tool to rebuild the index. Use _systemd-timer_ to run the tool regularly.

The system was still overloaded when exporting data or backing-up data.
