## Database issues

Second version: InfluxDB

- No need for additional tool to rebuild index.
- Querying data for other time range doesn't make CPU busier than recent time.
- Automatically summary old data (10s/sample → 10min/sample).
