## Objective of the control system

- Monitor the environmental conditions: temperature, humidity, brightness, carbon dioxide.
- Remotely turn on/off equipments like humidifier, light, air-cond, fan to adjust the environmental condition.
- Automatically turn on/off those equipments to reach desired conditions, suitable for mushroom growth.
