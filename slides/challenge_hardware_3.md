## Hardware specific issues

- BeagleBone doesn't have clock module → Doesn't update time when being off → Wrong time when boot up again. Need Internet to update system time.

- BeagleBone's Debian uses [ConnMan](https://01.org/connman) to manage network. Its DNS proxy doesn't work → breaks BeagleBone time (failed to synchronize with NTP server).

- Solved: [Disable](https://wiki.archlinux.org/index.php/Connman#Avoiding_conflicts_with_local_DNS_server) ConnMan's DNS proxy.
