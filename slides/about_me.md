## Who am I

- Free and Open-Source Software enthusiast.
- Improve OpenPGP card support for [OpenSC](https://github.com/OpenSC/OpenSC).
- Develop software for [OpenWrt](https://openwrt.org/) (router OS).
- Develop software for [Maemo](https://en.wikipedia.org/wiki/Maemo) (mobile OS).
- Web backend developer (PHP, Python).
- [Google Summer of Code](https://developers.google.com/open-source/gsoc/) mentor.
- Co-founder & CTO of [AgriConnect](http://agriconnect.vn), a young Vietnam-based agritech startup.
